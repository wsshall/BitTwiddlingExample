//============================================================================
// Name        : BitTwidlingProgram.cpp
// Author      : Wesley Shall
// Description : An example of bit twiddling, using bitwise shifts on an
//				 unsigned char.
//============================================================================

#include <iostream>
using namespace std;

/*Prints the binary representation of the char parameter
@param cValue: The char value to be printed in binary*/
void printBinary(unsigned char cValue) {
	//cout << "The size of cValue is " << sizeof(cValue) << endl;
	for (int i = 7; i >= 0; i--)
		cout << ((cValue >> i) & 1);
	cout << endl;
}

/*Shift the bits of the char parameter left by a specified amount
@param	cValue: The char value to be modified, unsigned to
				ensure the most significant bit does not
				remain set.
@param	shiftValue: The amount to shift cValue by
@return	newValue: The result of the shift operation*/
char shiftCharToLeft(unsigned char cValue, int shiftValue) {
	unsigned char newValue = cValue << shiftValue;
	return newValue;
}

/*Shift the bits of the char parameter right by a specified amount
@param	cValue: The char value to be modified, unsigned to
				ensure the most significant bit does not
				remain set.
@param	shiftValue: The amount to shift cValue by
@return	newValue: The result of the shift operation*/
char shiftCharToRight(unsigned char cValue, int shiftValue) {
	unsigned char newValue = cValue >> shiftValue;
	return newValue;
}

int main() {

	//int testValue = sizeof(char);
	//cout << value << endl;

	//Create a new char value and prints the char, decimal, and
	//binary representation of it
	unsigned char value = 'M';
	cout << value << endl;
	cout << (int) value << endl;
	printBinary(value);

	//Bit shift the char to the left and print the results
	value = shiftCharToLeft(value, 1);
	cout << value << endl;
	cout << (int) value << endl;
	printBinary(value);

	//Bit shift the char to the right and print the results
	value = shiftCharToRight(value, 1);
	cout << value << endl;
	cout << (int) value << endl;
	printBinary(value);

	return 0;
}
